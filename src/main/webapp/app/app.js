/**
 * Application main module
 */
angular.module('app', [ 'homePage', 'ui.bootstrap', 'ngRoute' ]);


angular.module('app').config(['$routeProvider', function($routeProvider) {

    $routeProvider.otherwise({
        redirectTo: '/home'
    });

}]);

angular.module('app').controller('nav.Controller', function($scope, $location) {

    $scope.$location = $location;

});
