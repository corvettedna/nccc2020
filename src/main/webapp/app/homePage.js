/**
 * Application dashboard module
 */
var myApp = angular.module('homePage', [ 'ngRoute', 'ngResource' ]);


myApp.config(['$routeProvider', function($routeProvider) {

    $routeProvider.when('/home', {
        templateUrl: 'app/webPages/homePage.html',
        controller: 'homePageController'
    });

}]);

myApp.controller('homePageController', [ '$scope', '$resource', '$location', '$window', '$rootScope', function($scope, $resource, $location, $window, $rootScope) {
	$rootScope.$on('$stateChangeSuccess', function (event) {
	    $window.ga('send', 'pageview', $location.path());
	});
}]);